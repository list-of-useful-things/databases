### Databases
* [CockroachDB](https://www.cockroachlabs.com/) / [github](https://github.com/cockroachdb)
* [rqlite](https://github.com/rqlite/rqlite) - The lightweight, distributed relational database built on SQLite

### Time-series databases & tools
* [RIAK](http://basho.com/products/#riak)
* [Graphite](https://en.wikipedia.org/wiki/Graphite_(software))
* [RRDtool](https://en.wikipedia.org/wiki/RRDtool)
* [InfluxDB](https://en.wikipedia.org/wiki/InfluxDB)
* [MEMSQL](https://www.memsql.com/)

### Tools
* [DBeaver](https://dbeaver.io/) - Free multi-platform database tool. Supports all popular databases: MySQL, PostgreSQL, MariaDB, SQLite, Oracle, DB2, SQL Server, Sybase, MS Access, Teradata, Firebird, Derby
* [OmniDB](https://omnidb.org/en/) - Open Source Collaborative Environment For Database Management
* [pgAdmin](https://www.pgadmin.org/) - pgAdmin is the most popular and feature rich Open Source administration and development platform for PostgreSQL
* [Draxed](https://www.draxed.com/) - A web based MySQL and PostgreSQL data browser and dashboard manager
* [DBGlass](https://electronjs.org/apps/dbglass) - Simple cross-platform PostgreSQL client

### Articles / Tutorials
* [It’s About Time For Time Series Databases](https://www.nextplatform.com/2018/01/25/time-time-series-databases/)
* [Working With Time Series Data](https://dzone.com/refcardz/working-with-time-series-data?chapter=1)
* [Kurs SQL / PL](https://www.sqlpedia.pl/kurs-sql/)
* [Life of a SQL query](https://numeracy.co/blog/life-of-a-sql-query)
* [Lessons learned scaling PostgreSQL database to 1.2bn records/month](https://medium.com/@gajus/lessons-learned-scaling-postgresql-database-to-1-2bn-records-month-edc5449b3067)
* [How to update objects inside JSONB arrays with PostgreSQL](https://medium.freecodecamp.org/how-to-update-objects-inside-jsonb-arrays-with-postgresql-5c4e03be256a)
* [PostgreSQL / Automating Postgres Indexing with Dexter & HypoPG](https://headway.io/blog/automating-postgres-indexing-with-dexter-and-hypopg/)

### Explained on images
* [SQL joins](https://i.redd.it/xd0kgoexwon21.jpg)